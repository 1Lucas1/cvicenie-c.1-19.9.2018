# """
# # Vaším úkolem je vytvořit textovou hru:
# # https://cs.wikipedia.org/wiki/Textov%C3%A1_hra
# #
# # Při tvorbě použijte proměnné, funkci input(), funkci print(), alespoň 4 podmínky (jedna z podmínek bude
# # obsahovat i elif větev a zanořenou podmínku včetně operatorů and a or).
# #
# # Můžete udělat hru na jakékoliv téma. Hlavně ať to není nudné a splní to minimální podmínky zadání!
# #
# # Celkem to bude za 5 bodů. Dalších 5 bonusových bodů (nezapočítávají se do základu)
# # můžete získat za správné použití 5 libovolných funkcí pro řětězce, čísla (např. split() apod.). Další funkce zkuste vygooglit na internetu.
# # Použité funkce se musí vztahovat k logice hry.
# # """

print("")
print("TEXTOVÁ HRA")
print("")

def start():
    print("Ako sa voláš?")

    meno = input()
    print("Ahoj",meno)
    print("""Bude ti vadiť, že táto hra je v slovenčine?
    Áno
    Nie""")

    volba = input()
    if volba == "Áno":
        zle()
    elif volba == "Nie":
        dobre()
    else:
        print("Zostal si v lodi, kam strielali nepriatelia. Zastrelili ťa. Si mŕtvy! Koniec hry")

def zle():
    print("""Tak to nehraj, ty hnusný šovinista! Hra končí!""")

def dobre():
    print("""Super, tak hra sa môže začať.""")
    print("Tvoj príbeh sa odohráva počas 2. svetovej vojny, počas operácie Overlord (Den D). Si americký vojak, \n"
          "ktorí sa spolu so svojou jednotkou chystáte vylodiť na pláži Omaha. Blížite sa k pláži, avšak už na vode\n"
          "na vás začnú strielať. Otvorí sa rampa vyloďovacej lode a prví muži začnú vybehovať na pláž.")
    print ("a = Vybehneš za nimi\n"
           "b = Prelezieš bok lode a skočíš do vody")

    volba = input()
    if volba == "a":
        mrtvi()
    elif volba == "b":
        zijes()
    else:
        print ("Zostal si v lodi a nikde si sa nekryl a tak ťa zastrelili. Si mŕtvi. Koniec hry!")

def mrtvi():
    print ("Nepriatelia mali zameraný predok lode a všetkých čo vychádzali tade zastrelili. Ty si bol medzi nimi. Si "
           "mŕtvy. Koniec hry!")

def zijes():
    print ("Mal si šťastie, voda nebola v týchto miestach hlboká a tak si sa neutopil. Nepriatelia hlavne strieľajú "
           "na predok lode a tak máš čas sa rozhodnúť čo ďalej.")
    print ("a = Pokračuješ vpred\n"
           "b = Tvoj kamarát je ranení, ideš mu pomôcť\n"
           "c = Si dezorientovaný a nevieš čo ďalej")

    volba = input ()
    if volba == "a":
        vpred()
    elif volba == "b":
        pomoc()
    elif volba == "c":
        smrt()

def vpred():
    print (
        'Pokračuješ vpred a snažíš sa nájsť nejaký úkryt. Vidíš pred sebou kráter a drevený plot. Kde sa schováš? ')
    print ("a = V kráteri\n"
           "b = Za plotom")

    volba = input ()
    if volba == "a":
        pokracujes()
    elif volba == "b":
        smrt()
    else:
        print ("Nevedel si sa rozhodnúť a tak ťa nakoniec zastrelili, si mŕtvy. Koniec hry!")

def smrt():
    print ("Zastrelili ťa, si mŕtvy. Koniec hry!")

def pomoc():
    print ("Prišiel si pomôcť kamarátovi.")
    print ("a = Ošetríš ho namieste\n"
           "b = Odtiahneš to do úkrytu a tam mu pomôžeš")

    volba = input()
    if volba == "a":
        smrt()
    elif volba == "b":
        osetrenie()

def osetrenie():
    print ("Odtiahol si kamaráta do úkrytu a tam si mu pomohol.")
    print ("a = Pokračuješ ďalej v boji\n"
           "b = Zostal si s ním")

    volba = input()
    if volba == "a":
        pokracujes()
    elif volba == "b":
        koniec()

def koniec():
    print("Zostal si pri kamarátovi. Zostal si pocelý čas s ním do kým neskončili boje.\n"
          "Vďaka tomu ste prežili obidvaja. Vyhral si! Koniec hry!")

def pokracujes():
    print ("Pokračuješ v boji. Začal si strieľať na nepriateľov. V nepriateľskom bunkri je kulometčík,\n"
           "ktorí strieľa na vaše pozície.")
    print("a = Hodíš do bunkru granát\n"
          "b = Zastrelíš ho puškou\n"
          "c = Pokúsiš sa dostať bližšie k bunkru")

    volba = input()
    if volba == "a":
        znicene()
    elif volba == "b":
        strela()
    elif volba == "c":
        smrt()
    else:
        print ("Zostal si schovaný a prečkal si boje. Prežil si. Vyhral si! Koniec hry!")

def strela():
    print("Netrafil si a kulometčík si ťa všimol a zastrelil ťa. Si mŕtvy! Koniec hry!")

def znicene():
    print("Granát zabil kulometčíka a zničil kulomet. Zničil si tak posledné miesto obrany nepriateľa.\n"
          "Prežil si celú operáciu. Vyhral si! Koniec hry!")

start()

