# 20 bodu
'''Kdo hral textovou hru od Googlu tak bude v obraze. Cilem bude udelat mapu. Muzete si udelat vlastni nebo pouzit moji.
Mapa se bude skládat z několika částí a bude reprezentována textem. Obrázek mapy se nachází pod tímto textem
Cilem toho cviceni bude spise vymyslet jak toto cviceni vyresit. Samotna implementace nebude tak narocna.

Prace s dvourozmernym polem je seznamem (polem) je jednoducha

pole = [['a','b','c','d'],
        ['e','f','g','h'],
        ['i','j','k','l']]

Dvourozmerne pole obsahuje radky a sloupce. Kazda hodnota ma svuj jedinecny index definovany indexem radku a sloupce
napr. pole[0][1] vybere hodnotu na prvni radku ve druhem sloupci 'b'.
      pole[1][3] vybere hodnotu na druhem radku ve ctvrtem sloupci 'h'.
Vice informaci najdete na internetu: https://www.tutorialspoint.com/python/python_2darray.htm

Mapa (dvourozmerne pole) predstavuje bludiste. Vase postava se na zacatku hry bude nachazet na pozici pismene S (index [7][0]).
Cílem je dostat se do cile, ktery oznacuje pismeno F (index [0][9]).
Budete se ptat uzivatele stejne jako v textove hre na informaci o tom kterym smerem chce jit - nahoru, doprava, dolu, doleva.
Po každém zadání posunu vykreslíte mapu, kde bude zobrazena pozice postavicky v mape a pozici v jejim primem okoli. Bude to vypadat asi takto:

      ?????
      ?***?
      ?*P.?
      ?*.*?
      ?????

Tečky představují cestu (místo kam může postavička stoupnout). Hvězdičky představují zeď, tam postavička nemůže vstoupit.
Ostatní okolí postavy (zbytek mapy) bude ukryt postavě a budou ho představovat otazníky.

# BONUS: 5 bodů
# Udělejte verzi, kde se bude ukládat a při vykreslování mapy zobrazovat i místo, které postava už prošla a tudíž ho zná.
'''

print("TEXTOVÉ BLÚDISKO")
def mapa():
    hra = [['▓', '▓', '▓', '▓', '▓', '▓', '  ', '  ', ' ', 'F'],
                ['▓', '▓', '▓', '▓', '  ', ' ', ' ', '▓', '▓', '▓'],
                ['  ', '  ', '  ', ' ', ' ', '▓', '▓', '▓', '▓', '▓'],
                [' ', '▓', '▓', '▓', '▓', '▓', '▓', '▓', '  ', '  '],
                [' ',  '  ', ' ', '▓', '▓', '  ', '  ', '  ', '  ', '  '],
                ['▓', '▓', ' ', ' ', '  ', '  ', '▓', '▓', '▓', '▓'],
                ['▓', '▓', ' ', '▓', '▓', '▓', '▓', '▓', '▓', '▓'],
                ['S ', '  ', '  ', ' ', ' ', '  ', '  ', '  ', '  ', '  ']]

    hranie = [['░', '░', '░', '░', '░', '░', '░', '░','░','░'],
                 ['░', '░', '░', '░', '░', '░', '░', '░','░','░'],
                 ['░', '░', '░', '░', '░', '░', '░', '░','░','░'],
                 ['░', '░', '░', '░', '░', '░', '░', '░','░','░'],
                 ['░', '░', '░', '░', '░', '░', '░', '░','░','░'],
                 ['░', '░', '░', '░', '░', '░', '░', '░','░','░'],
                 ['░', '░', '░', '░', '░', '░', '░', '░','░','░'],
                 ['S ', '░', '░', '░', '░', '░', '░', '░', '░', '░']]

    riadok = 7
    stlpec = 0

    if riadok != 7:
        hranie[riadok + 1][stlpec] = hra[riadok + 1][stlpec]
    if riadok != 0:
        hranie[riadok - 1][stlpec] = hra[riadok - 1][stlpec]
    if stlpec != 9:
        hranie[riadok][stlpec + 1] = hra[riadok][stlpec + 1]
    if stlpec != 0:
        hranie[riadok][stlpec - 1] = hra[riadok][stlpec - 1]

    for zoznam in hranie:
        print(" ".join(zoznam))

    while (riadok != 0) or (stlpec != 9):
        uvod = input("Kam chceš ísť?\nhore\ndole\ndolava\ndoprava\n")
        print("")

        if uvod == "hore":
            riadok2 = riadok - 1
            stlpec2 = stlpec
        elif uvod == "dole":
            riadok2 = riadok + 1
            stlpec2 = stlpec
        elif uvod == "dolava":
            riadok2 = riadok
            stlpec2 = stlpec - 1
        elif uvod == "doprava":
            riadok2 = riadok
            stlpec2 = stlpec + 1
        else:
            print("Zle si napísal, znova!")
            continue

        if riadok2 >= len(hra) or riadok2 < 0 or stlpec2 >= len(hra[0]) or stlpec2 < 0:
            print("Vyšiel si mimo hraciu plochu, znova!")
        elif hra[riadok2][stlpec2] == "▓":
            print("Narazil si do steny, znova!")
        else:
            hranie[riadok2][stlpec2] = "P"

            riadok = riadok2
            stlpec = stlpec2

            if riadok2 != len(hra) - 1:
                hranie[riadok2 + 1][stlpec2] = hra[riadok2 + 1][stlpec2]
            if riadok2 != 0:
                hranie[riadok2 - 1][stlpec2] = hra[riadok2 - 1][stlpec2]
            if stlpec2 != len(hra[0]) - 1:
                hranie[riadok2][stlpec2 + 1] = hra[riadok2][stlpec2 + 1]
            if stlpec2 != 0:
                hranie[riadok2][stlpec2 - 1] = hra[riadok2][stlpec2 - 1]

        for zoznam in hranie:
            print(" ".join(zoznam))

    print("Prešiel si blúdískom. Vyhral si!")

mapa()